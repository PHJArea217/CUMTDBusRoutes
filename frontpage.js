var frontpage = {
	frontText: function(screen) {
		var x = "<p>Select 'Routes', 'Stops', or 'By Distance' from above.</p>";
		var doI = false;
		switch (screen) {
			case "routes-s":
				doI = true;
			case "routes":
				x = "";
				break;
			case "home":
				x = x + '<p><img src="img/bus-1.jpg" /></p>';
		}
		x = x + `
<div class="frontpage-notice">
As of 12 August 2020, this app will still receive periodic updates. However, due to the COVID-19 pandemic and recent server upgrades, real-time departure information is currently unavailable.
`
		if (!doI) x += `
`;
		x = x + '</div>';
		return x;
	}
};
export default frontpage;
