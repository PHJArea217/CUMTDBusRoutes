#!/bin/sh

set -eu
write_metadata() {
	cat > "public/${2:-metadata}.js" <<EOF
document.getElementById("last-build-time").innerHTML = "$(date -u +'%F %T')";
document.getElementById("last-gtfs-update").innerHTML = "$(TZ=Etc/UTC stat -c%y ${1:-google_transit.zip} | head -c 19)";
EOF
}

if ! [ -f calendar.txt ]; then
	wget -O gt-tmp https://developer.mtd.org/gtfs/google_transit.zip && mv gt-tmp google_transit.zip
	busybox unzip google_transit.zip routes.txt stops.txt stop_times.txt trips.txt calendar_dates.txt calendar.txt
fi
javac BusRoutes.java && java BusRoutes
write_metadata
cat header.html index-dynamic.html footer.html > public/about.html
node_modules/webpack-cli/bin/cli.js
cp csp.js frontpage.css bus_routes.css busroutes-bundled.js public/ && grep -v '@@delete on production@@' bus_routes.html > public/index.html
mkdir public/img && cp *.jpg public/img/
