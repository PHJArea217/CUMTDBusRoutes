const alias_mapping = require("./_alias-mapping.json");
const axios = require('axios');
const config = require('./config.json');
var currentState = {};

function initCurrentState(override_alias_mapping) {
	let mapping = override_alias_mapping || alias_mapping;
	for (let x of ['routes', 'stops', 'trips', 'service', 'blocks']) {
		let m = new Map();
		for (let e of mapping[x]) {
			m.set(e[1], e[0]);
		}
		currentState['data_' + x] = m;
	}
	currentState.currentVehicles = null;
}

function transformMap(whichMap, originalKey) {
	if (originalKey === null) return null;
	let result = whichMap.get(originalKey);
	if (!result) {
		return "!" + originalKey;
	}
	return result;
}
function transformTime(orig_date) {
	try {
		return new Date(orig_date).toISOString();
	} catch (e) {
		return "(unknown)";
	}
}
function transformVehiclesJson(orig_getvehicles_output) {
	let result = [];
	for (let v of orig_getvehicles_output.vehicles) {
		result.push({
			vehicle_id: v.vehicle_id,
			previous_stop_id: transformMap(currentState.data_stops, v.previous_stop_id),
			next_stop_id: transformMap(currentState.data_stops, v.next_stop_id),
			origin_stop_id: transformMap(currentState.data_stops, v.origin_stop_id),
			destination_stop_id: transformMap(currentState.data_stops, v.destination_stop_id),
			trip: {
				trip_id: transformMap(currentState.data_trips, v.trip.trip_id),
				route_id: transformMap(currentState.data_routes, v.trip.route_id),
				block_id: transformMap(currentState.data_blocks, v.trip.block_id),
				service_id: transformMap(currentState.data_service, v.trip.service_id)
			},
			last_updated: transformTime(v.last_updated)
		});
	}
	return {time: transformTime(orig_getvehicles_output.time), vehicles: result};
}
function main_loop_iteration_40s() {
	axios.get('https://developer.mtd.org/api/v2.2/json/getvehicles?key=' + config.key).then(function (response) {
		if (response.status === 200) {
//			console.log(response.data);
			try {
				currentState.currentVehicles = transformVehiclesJson(response.data);
			} catch (e) {
			}
		}
	}).catch(function(err) {});
}
function make_express_app(app) {
	app.get('/vehicles.json', (req, res) => {
		res.set('access-control-allow-origin', '*');
		res.set('cache-control', 'private, max-age=10');
		res.send(200, currentState.currentVehicles);
	});
	let loop = function () {
		setTimeout(loop, 37000);
		main_loop_iteration_40s();
	};
	process.nextTick(loop);
}
initCurrentState(null);
exports.main_loop_iteration_40s = main_loop_iteration_40s;
exports.currentState = currentState;
exports.make_express_app = make_express_app;
undefined;
