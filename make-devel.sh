#!/bin/sh
set -eu
mkdir -p devel
cd devel
for x in about.html bus_routes.css csp.js frontpage.css img meta metadata.js routes trip-blocks; do ln -sfT ../public/"$x" "$x"; done
for x in bus_routes.js mtd br_rt_departures.js frontpage.js; do ln -sfT ../"$x" "${x##*/}"; done
ln -sfT ../bus_routes.html index.html
